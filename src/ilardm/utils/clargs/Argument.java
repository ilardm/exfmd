// Copyright (c) 2013, Ilya Arefiev <arefiev.id@gmail.com>
// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//  * Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//  * Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the
//    distribution.
//  * Neither the name of the author nor the names of its
//    contributors may be used to endorse or promote products derived
//    from this software without specific prior written permission.

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
// IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

package ilardm.utils.clargs;

public class Argument {
    public interface Interface {
        public String toString();
        public Argument getValue();
    };

    public enum MATCH { NONE, SHORT, LONG };

    // =========================================================================

    public final String SHORT;
    public final String LONG;
    public final boolean SINGLE_USE;
    public final boolean REQUIRES_VALUE;
    public final String DESCRIPTION;

    // -------------------------------------------------------------------------

    public Argument( String _short, String _long, String _desc, boolean _single, boolean _value ) {
        SHORT = _short;
        LONG = _long;
        DESCRIPTION = _desc;
        SINGLE_USE = _single;
        REQUIRES_VALUE = _value;
    }

    public MATCH matches( String _s ) {
        MATCH ret = MATCH.NONE;

        // TODO: rewrite
        if ( REQUIRES_VALUE ) {
            if ( _s.startsWith( SHORT.concat( "=" ) ) ) {
                ret = MATCH.SHORT;
            } else if ( _s.startsWith( LONG.concat( "=" ) ) ) {
                ret = MATCH.LONG;
            }
        } else {
            if ( _s.equals( SHORT ) ) {
                ret = MATCH.SHORT;
            } else if ( _s.equals( LONG ) ) {
                ret = MATCH.LONG;
            }
        }

        return ret;
    }

    public String toString() {
        return ( SHORT
                 + "/"
                 + LONG
                 + (REQUIRES_VALUE ? "=v1,v2,..." : "")
                 + "\t- "
                 + DESCRIPTION
            );
    }
}
