// Copyright (c) 2013, Ilya Arefiev <arefiev.id@gmail.com>
// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//  * Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//  * Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the
//    distribution.
//  * Neither the name of the author nor the names of its
//    contributors may be used to endorse or promote products derived
//    from this software without specific prior written permission.

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
// IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

package ilardm.exfmd;

// http://hc.apache.org/httpcomponents-client-ga/apidocs/
import org.apache.http.*;
import org.apache.http.client.*;
import org.apache.http.client.methods.*;
import org.apache.http.impl.client.DefaultHttpClient;
import java.io.*;
import org.json.*;
import java.util.Vector;

class Fetcher {
    public enum Targets {
        BLUES( "blues" )
            , CHILLWAVE( "chillwave" )
            , CLASSICAL( "classical" )
            , COUNTRY( "country" )
            , DUBSTEP( "dubstep" )
            , ELECTRONICA( "electronica" )
            , EXPERIMENTAL( "experimental" )
            , FOLK( "folk" )
            , HIP_HOP( "hip-hop" )
            , HOUSE( "house" )
            , INDIE( "indie" )
            , JAZZ( "jazz" )
            , MASHUP( "mashup" )
            , METAL( "metal" )
            , POP( "pop" )
            , PUNK( "punk" )
            , REGGAE( "reggae" )
            , ROCK( "rock" )
            , SHOEGAZE( "shoegaze" )
            , SOUL( "soul" )
            , SYNTHPOP( "synthpop" )
            , OVERALL( "" );

        private final String TARGET;
        private final String API_TARGET;

        // ---------------------------------------------------------------------

        private Targets( String _target ) {
            if ( _target.isEmpty() ) {
                TARGET = "overall";
                API_TARGET = _target;
            } else {
                TARGET = _target;
                API_TARGET = "/tag/" + _target;
            }
        }

        public String toString() {
            return TARGET;
        }

        public String getValue() {
            return API_TARGET;
        }
    }

    // =========================================================================

    public class Item {
        public final String artist;
        public final String album;
        public final String title;
        public final String url;
        public final Targets target;
        public final int number;

        // ---------------------------------------------------------------------

        public Item( int _num, String _art, String _alb, String _ttl, String _url, Targets _target ) {
            number = _num;
            artist = _art;
            album = _alb;
            title = _ttl;
            url = _url;
            target = _target;
        }

        public String toString() {
            StringBuffer b = new StringBuffer();

            b.append( target );
            b.append( "-" );
            b.append( String.format( "%02d", number ) );
            b.append( "-" );
            b.append( artist );
            b.append( "-" );
            b.append( album );
            b.append( "-" );
            b.append( title );

            return b.toString();
        }
    };

    // =========================================================================

    private static final String API_URL = "http://ex.fm/api/v3";
    private static final String TRENDING_URL = "/trending";

    private HttpClient hc = new DefaultHttpClient();
    private Vector<Item> dlist = new Vector<Item>();

    // -------------------------------------------------------------------------

    public Fetcher() {
    }

    public boolean fetch() {
        return fetch( Targets.OVERALL );
    }

    public boolean fetch( Targets _target ) {
        boolean ret = true;
        String url = API_URL.concat( TRENDING_URL ).concat( _target.getValue() );

        System.out.println( "fetching "
                            + _target
                            + " list"
                            + " (url: "
                            + url
                            + ")"
            );

        // fetch trending
        HttpGet gm = new HttpGet( url );
        // gm.setHeader( "Referer", "http://ex.fm/static/html/player.html" );
        gm.setHeader( "User-Agent", "Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.97 Safari/537.11" );

        HttpResponse rsp = null;
        HttpEntity ent = null;
        String rspJs = null;

        try {
            rsp = hc.execute( gm );

            if ( rsp.getStatusLine().getStatusCode() != HttpStatus.SC_OK ) {
                throw new Exception( "bad status code: "
                                     + rsp.getStatusLine().getStatusCode()
                    );
            }

            ent = rsp.getEntity();

            System.out.println( "ctype: " + ent.getContentType() );
            System.out.println( "clen: " + ent.getContentLength() );

            BufferedReader reader = new BufferedReader(
                new InputStreamReader(
                    ent.getContent()
                    )
                );
            StringBuffer b = new StringBuffer();
            String l = null;

            // TODO: read bytes instead of lines
            while ( (l = reader.readLine()) != null )
                b.append( l ).append( "\n" );

            rspJs = b.toString();
            reader.close();
        } catch ( Exception e ) {
            System.err.println( "oops: "
                                + e.getMessage()
                );
            e.printStackTrace();

            ret = false;
        } finally {
            gm.abort();
        }

        if ( !ret )
            return ret;

        // parse trending
        try {
            JSONObject rspJo = new JSONObject( rspJs );
            int cnt = rspJo.getInt( "results" );
            JSONArray songs = rspJo.getJSONArray( "songs" );

            for ( int i = 0; i < cnt; ++i ) {
                JSONObject song = songs.getJSONObject( i );

                Item itm = new Item( i
                                     , song.getString( "artist" )
                                     , song.getString( "album" )
                                     , song.getString( "title" )
                                     , song.getString( "url" )
                                     , _target
                    );
                dlist.add( itm );
                System.out.println( "#" + i + " "
                                    + itm
                                    + " @ " + itm.url
                    );
            }
        } catch ( Exception e ) {
            System.err.println( "oops: "
                                + e.getMessage()
                );
            e.printStackTrace();

            ret = false;
        }

        return ret;
    }

    public Vector<Item> getDlist() {
        return dlist;
    }
}
