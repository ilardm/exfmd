// Copyright (c) 2013, Ilya Arefiev <arefiev.id@gmail.com>
// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//  * Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//  * Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the
//    distribution.
//  * Neither the name of the author nor the names of its
//    contributors may be used to endorse or promote products derived
//    from this software without specific prior written permission.

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
// IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

package ilardm.exfmd;

import java.util.Vector;
import java.util.ArrayList;
import java.util.concurrent.ArrayBlockingQueue;
import org.apache.http.*;
import org.apache.http.client.*;
import org.apache.http.client.methods.*;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.commons.io.IOUtils;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.io.File;

class Downloader {
    private class Worker extends Thread {
        private ArrayBlockingQueue<Fetcher.Item> itemQ = null;
        private HttpClient hc = null;
        private HttpGet gm = null;
        private HttpResponse rsp = null;
        private HttpEntity ent = null;
        private long id;
        private String directory = null;
        private final String SAFE_PATTERN = new String( "[^a-zA-Z0-9_.()]+?" );
        private final String SEPARATOR = new String( "/" );
        private final String EXT = new String( ".mp3" );
        private final int SAFE_LEN = 255 - EXT.length();

        // ---------------------------------------------------------------------

        public Worker( int _items ) {
            itemQ = new ArrayBlockingQueue<Fetcher.Item>( _items );
            hc = new DefaultHttpClient();
            id = getId();

            directory = (new SimpleDateFormat( "yyyyMMdd" )).format( new Date());
            File dir = new File( directory );
            if ( !dir.exists() ) {
                try {
                    dir.mkdir();
                } catch ( Exception e ) {
                    System.out.println( "oops: "
                                        + e.getMessage()
                        );
                    e.printStackTrace();
                }
            }
        }

        public void addItem( Fetcher.Item _item ) {
            synchronized( itemQ ) {
                try {
                    itemQ.add( _item );
                } catch ( Exception e ) {
                    System.err.println( "[" + id + "] "
                                        + "oops: "
                                        + e.getMessage()
                        );
                    e.printStackTrace();
                }
            }
        }

        private String safeFilename( String _filename ) {
            String ret = _filename;
            try {
                ret = _filename.replaceAll( SAFE_PATTERN, "_" );
            } catch ( Exception e ) {
                System.err.println( "[" + id + "] "
                                    + "oops: "
                                    + e.getMessage()
                    );
                e.printStackTrace();
            }

            int len = (ret.length() < SAFE_LEN ? ret.length() : SAFE_LEN );
            return ret.substring(0, len);
        }

        @Override
        public void run() {
            Fetcher.Item itm = null;

            final int qsize;
            int inum = 0;
            synchronized ( itemQ ) {
                qsize = itemQ.size();
            }

            do {
                synchronized( itemQ ) {
                    itm = itemQ.poll();
                }

                if ( itm == null ) {
                    System.out.println( "[" + id + "] "
                                        + "Q is empty"
                        );
                    return;
                }

                System.out.println( "[" + id + "] "
                                    + "processing item "
                                    + (++inum) + "/" + qsize
                                    + " '" + itm + "'"
                    );

                gm = new HttpGet( itm.url );
                gm.setHeader( "Referer", "http://ex.fm/static/html/player.html" );
                gm.setHeader( "User-Agent", "Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.97 Safari/537.11" );

                try {
                    rsp = hc.execute( gm );
                    if ( rsp.getStatusLine().getStatusCode() != HttpStatus.SC_OK ) {
                        String emsg = new String( "http:"
                                                  + rsp.getStatusLine().getStatusCode()
                                                  + " for url '"
                                                  + itm.url + "'"
                            );
                        throw new Exception( emsg );
                    }

                    ent = rsp.getEntity();
                    System.out.println( "[" + id + "] "
                                        + "ctype: "
                                        + ent.getContentType()
                        );
                    System.out.println( "[" + id + "] "
                                        + "clen: "
                                        + ent.getContentLength()
                        );

                    FileOutputStream os = new FileOutputStream( directory
                                                                + SEPARATOR
                                                                + safeFilename( itm.toString() )
                                                                + EXT
                        );
                    IOUtils.copy( ent.getContent(), os );
                    os.close();
                } catch ( Exception e ) {
                    System.out.println( "[" + id + "] "
                                        + "oops: "
                                        + e.getMessage()
                        );
                    e.printStackTrace();
                } finally {
                    gm.abort();
                }
            } while ( true );
        }
    }

    // =======================================================================

    private final Vector<Fetcher.Item> dlist;
    private ArrayList<Worker> workers = new ArrayList<Worker>();

    // -----------------------------------------------------------------------

    public Downloader( Vector<Fetcher.Item> _dlist ) {
        dlist = _dlist;

        int cpu = Runtime.getRuntime().availableProcessors();
        for ( int i = 0; i < cpu; ++i ) {
            workers.add( new Worker( (dlist.size() / cpu + 1) ) );
        }

        for ( int i = 0, w = 0; i < dlist.size(); ++i, w = i % cpu ) {
            System.out.println( "put item '"
                                + dlist.elementAt(i)
                                + "' into queue #"
                                + w
                );
            workers.get(w).addItem( dlist.elementAt( i ) );
        }
    }

    public void download() {
        System.out.println( "start download process" );
        for ( Worker w: workers ) {
            w.start();
        }

        boolean alive;
        do {
            alive = false;
            for ( Worker w: workers ) {
                alive |= w.isAlive();
            }

            try {
                Thread.sleep(100);
            } catch ( Exception e ) {}
        } while ( alive );

        System.out.println( "downloads complete" );
        for ( Worker w: workers ) {
            try {
                w.join();
            } catch ( Exception e ) {
                System.err.println( "oops: "
                                    + e.getMessage()
                    );
                e.printStackTrace();
            }
        }
    }
};
