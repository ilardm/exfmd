// Copyright (c) 2012, Ilya Arefiev <arefiev.id@gmail.com>
// All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//  * Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//  * Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the
//    distribution.
//  * Neither the name of the author nor the names of its
//    contributors may be used to endorse or promote products derived
//    from this software without specific prior written permission.

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
// IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
// PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

package ilardm.ant;

import org.apache.tools.ant.Task;
import org.apache.tools.ant.BuildException;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class VcTask extends Task {
    private String base = null;
    private String target = null;
    private String version = null;
    private String vctype = "";
    private boolean devel = true;

    public void execute() {
        log("base:\t" + base + "\n" + "target:\t" + target + "\n"
                + "version:\t" + version);
        if (!setVersion()) {
            throw new BuildException("Unable to change version");
        }
    }

    public void setBase(String base) {
        this.base = base;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    private String getVersion() {
        String chageset = "000000000000";
        BufferedReader reader = null;
        String line = null;

        log("try hg");
        try {
            String cmd = "hg identify -it --cwd " + base;

            log("cmd: " + cmd);

            Process hg = Runtime.getRuntime().exec(cmd);
            reader = new BufferedReader(new InputStreamReader(
                    hg.getInputStream()));

            line = reader.readLine();
            reader.close();

            if (line != null) {
                String[] lineArray = line.split( " " );

                if ( lineArray.length > 1 ) {
                    if ( !lineArray[1].equals( "tip" ) ) {
                        devel = false;
                    }

                    line = lineArray[0];
                }

                if ( line.contains( "+" ) ) {
                    devel = true;
                    line = line.replace( "+", "" );
                }

                chageset = line;
                vctype = "hg";

                log("changeset from hg: " + chageset);

                return chageset;
            }
        } catch (Exception e) {}

        log("try hg_archival");
        try {
            String path = base + "/.hg_archival.txt";

            log("path: " + path);

            reader = new BufferedReader(new InputStreamReader(
                    new FileInputStream(path)));

            while ((line = reader.readLine()) != null) {
                if (line.startsWith("node")) {
                    break;
                }
            }
            reader.close();

            if (line.startsWith("node")) {
                line = line.replace("node: ", "");
                chageset = line.substring(0, chageset.length());

                vctype = "hg";

                log("changeset from hg_archival: " + chageset);

                return chageset;
            }
        } catch (Exception e) {}

        log("try git");
        try {
            String cmd = "git log -n1 --pretty=format:%h";

            log("cmd: " + cmd);

            Process git = Runtime.getRuntime().exec(cmd);
            reader = new BufferedReader(new InputStreamReader(
                    git.getInputStream()));

            line = reader.readLine();
            reader.close();

            if (line != null) {
                chageset = line.replace("+", "");

                vctype = "git";

                log("changeset from git: " + chageset);

                return chageset;
            }
        } catch (Exception e) {}

        log("no VCS found");

        return chageset;
    }

    private boolean setVersion() {
        String chageset = getVersion();
        StringBuffer output = new StringBuffer();

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    new FileInputStream(target)));
            String line = null;
            String[] parts = null;

            log("rading target " + target);

            while ((line = reader.readLine()) != null) {
                if (line.contains("VERSION_NUM =")) {
                    parts = line.split(" ");
                    parts[parts.length - 1] = version + "F;";
                } else if (line.contains("VERSION_VC =")) {
                    parts = line.split(" ");
                    parts[parts.length - 1] = "\"" + chageset +
                            ( devel ? "-dev" : "" ) + "\";";
                } else if (line.contains("VERSION_VC_TYPE =")) {
                    parts = line.split(" ");
                    parts[parts.length - 1] = "\"" + vctype + "\";";
                }
                if (parts != null) {
                    for (String i : parts) {
                        output.append(i + " ");
                    }
                    parts = null;
                } else {
                    output.append(line);
                }
                output.append("\n");
            }
            reader.close();

            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(target)));

            log("updating target " + target);

            writer.write(output.toString());
            writer.flush();
            writer.close();

            log("done");

            return true;
        } catch (Exception e) {}

        return false;
    }
}
